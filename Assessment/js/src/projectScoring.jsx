var CriteriumRow = require('./criteriumRow.jsx');

module.exports = React.createClass({
    getInitialState() {
        var totalScore = 0;

        for (var i=0; i < this.props.projectScoring.length; i++) {
            totalScore += this.props.projectScoring[i].Score;
        }

        return {
            projectScore : totalScore,
            projectScoring : this.props.projectScoring
        }
    },

    render() {
        var criteria = this.state.projectScoring.map((item) => {
            return <CriteriumRow c={item} i={this.state.projectScoring.indexOf(item)} onChange={this.scoreChanged} />
        });

        return (
            <div>
                <div>
                    <table className="table table-responsive table-striped">
                        <thead>
                        <tr>
                        <th>Item</th>
                        <th>Answer</th>
                        <th>Score</th>
                        </tr>
                        </thead>
                        <tbody>
                        {criteria}
                        <tr>
                            <td/>
                            <td><b>Total</b></td>
                            <td>{this.state.projectScore}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        );
    },

    scoreChanged(index, score) {
        this.state.projectScoring[index].Score = parseInt(score);

        var total = 0;

        for (var i=0; i < this.state.projectScoring.length; i++) {
            total += this.state.projectScoring[i].Score;
        }

        this.setState({
            projectScore : total
        })
    }

});