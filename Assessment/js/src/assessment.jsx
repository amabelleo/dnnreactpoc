﻿var QualifyingCost = require('./qualifyingCost.jsx');
var ProjectScoring = require('./projectScoring.jsx');

module.exports = React.createClass({

    getInitialState() {
        return {
            projectCosts: this.props.assessment.AnnexA,
            projectScoring: this.props.assessment.ProjectScoring,
            questions: this.props.assessment.Questions
        }
    },

    render() {
        var savePanel = this.props.security.IsAssessor ? (
            <div className="row">
                <div className="col-sm-12">
                <a href="#" className="btn btn-primary" onClick={this.saveClick}>Save</a>
                </div>
            </div>
        ) : null;

        return (
          <div>
            <p>Application number: {this.props.assessment.ApplicationNumber}</p>
            <p>Project name: {this.props.assessment.ProjectName}</p>
            <QualifyingCost projectCosts={this.state.projectCosts} />
            <ProjectScoring projectScoring={this.state.projectScoring} />
            {savePanel}
        </div>
        );
    },

    saveClick(e) {
        e.preventDefault();
        this.props.service.save(
            {
                ApplicationNumber: this.props.assessment.ApplicationNumber,
                ProjectCosts: this.state.projectCosts,
                ProjectScoring: this.state.projectScoring
            }
        );
    }

});