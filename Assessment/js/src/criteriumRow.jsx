module.exports = React.createClass({

    getInitialState() {
        return {
        }
    },

    render() {
        return (
        <tr>
            <td>{this.props.c.Criterium}</td>
            <td>{this.props.c.Answer}</td>
            <td>        
                <input
                type="text"
                ref="numScore"
                value={this.props.c.Score}
                onChange={this.change}
                />
            </td>
        </tr>
        );
    },

    change() {

        this.props.onChange(this.props.i, this.refs.numScore.value);
    }

});
