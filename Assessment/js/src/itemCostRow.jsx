module.exports = React.createClass({

    getInitialState() {
        return {
            isDisqualified : this.props.itemCost.Disqualify
        }
    },

    render() {
        return (
        <tr>
            <td>{this.props.itemCost.Description}</td>
            <td>{this.props.itemCost.Cost}</td>
            <td>        
                <input
                type="checkbox"
                checked={this.state.isDisqualified}
                onChange={this.change}
                />
            </td>
        </tr>
        );
    },

    change() {
        this.setState({
            isDisqualified : !this.state.isDisqualified
        });

        this.props.onChange(this.props.index);
    }

});
