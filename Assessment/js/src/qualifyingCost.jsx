var ItemCostRow = require('./itemCostRow.jsx');

module.exports = React.createClass({
    getInitialState() {
        var totalCost = 0;

        for (var i = 0; i < this.props.projectCosts.length; i++) {
            if (!this.props.projectCosts[i].Disqualify) {
                totalCost += this.props.projectCosts[i].Cost;
            }
        }

        return {
            qualifyingCost : totalCost,
            projectCosts : this.props.projectCosts
        }
    },

    render() {


        var itemCostRows = this.state.projectCosts.map((item) => {
            return <ItemCostRow itemCost={item} index={this.state.projectCosts.indexOf(item)} onChange={this.qualifyChanged} />
        });

        return (
            <div>
            <div>

                Qualifying Cost (See <a href="#" onClick={this.qualifyClick}>Annex A</a> to qualify/disqualify items)
                
                <p>{this.state.qualifyingCost}</p>
            </div>

            <div className="modal fade" id="qualifyCosts" tabindex="-1" role="dialog" aria-labelledby="editRideLabel">
                <div className="modal-dialog" role="document">
                <div className="modal-content">
                <div className="modal-header">
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 className="modal-title" id="annexALabel">Annex A</h4>
                </div>
                <div className="modal-body">
                    <table className="table table-responsive table-striped">
                        <thead>
                        <tr>
                        <th>Description</th>
                        <th>Cost</th>
                        <th>Disqualify</th>
                        </tr>
                        </thead>
                        <tbody>
                        {itemCostRows}
                        </tbody>
                    </table>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" className="btn btn-primary" onClick={this.updateCost}>Save</button>
                </div>
                </div>
                </div>
            </div>
            </div>
        );
    },

    qualifyClick(e) {
        e.preventDefault();
        $('#qualifyCosts').modal('show');
    },

    qualifyChanged(i) {
        this.state.projectCosts[i].Disqualify = !this.state.projectCosts[i].Disqualify;
    },

    updateCost(e) {
        e.preventDefault();
        $('#qualifyCosts').modal('hide');

        var updatedCost = 0;

        for (var i = 0; i < this.state.projectCosts.length; i++) {
            if (!this.state.projectCosts[i].Disqualify) {
                updatedCost += this.state.projectCosts[i].Cost;
            }
        }

        this.setState({
            qualifyingCost : updatedCost
        })
    }
});