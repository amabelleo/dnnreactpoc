﻿var Assessment = require('./assessment.jsx');

(function($, window, document, undefined) {

    $(document).ready(function() {
        $('.Assessment').each(function(i, el) {
            ReactDOM.render(<Assessment assessment={$(el).data('assessment')}
                                        security={$(el).data('security')} 
                                        service={new window.AssessmentService($, $(el).data('moduleid'))}/>, el);
    });
});

})(jQuery, window, document);
