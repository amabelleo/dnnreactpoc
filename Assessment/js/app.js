(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var Assessment = require('./assessment.jsx');

(function ($, window, document, undefined) {

    $(document).ready(function () {
        $('.Assessment').each(function (i, el) {
            ReactDOM.render(React.createElement(Assessment, { assessment: $(el).data('assessment'),
                security: $(el).data('security'),
                service: new window.AssessmentService($, $(el).data('moduleid')) }), el);
        });
    });
})(jQuery, window, document);

},{"./assessment.jsx":2}],2:[function(require,module,exports){
'use strict';

var QualifyingCost = require('./qualifyingCost.jsx');
var ProjectScoring = require('./projectScoring.jsx');

module.exports = React.createClass({
    displayName: 'exports',
    getInitialState: function getInitialState() {
        return {
            projectCosts: this.props.assessment.AnnexA,
            projectScoring: this.props.assessment.ProjectScoring,
            questions: this.props.assessment.Questions
        };
    },
    render: function render() {
        var savePanel = this.props.security.IsAssessor ? React.createElement(
            'div',
            { className: 'row' },
            React.createElement(
                'div',
                { className: 'col-sm-12' },
                React.createElement(
                    'a',
                    { href: '#', className: 'btn btn-primary', onClick: this.saveClick },
                    'Save'
                )
            )
        ) : null;

        return React.createElement(
            'div',
            null,
            React.createElement(
                'p',
                null,
                'Application number: ',
                this.props.assessment.ApplicationNumber
            ),
            React.createElement(
                'p',
                null,
                'Project name: ',
                this.props.assessment.ProjectName
            ),
            React.createElement(QualifyingCost, { projectCosts: this.state.projectCosts }),
            React.createElement(ProjectScoring, { projectScoring: this.state.projectScoring }),
            savePanel
        );
    },
    saveClick: function saveClick(e) {
        e.preventDefault();
        this.props.service.save({
            ApplicationNumber: this.props.assessment.ApplicationNumber,
            ProjectCosts: this.state.projectCosts,
            ProjectScoring: this.state.projectScoring
        });
    }
});

},{"./projectScoring.jsx":6,"./qualifyingCost.jsx":7}],3:[function(require,module,exports){
'use strict';

function isInt(value) {
    return !isNaN(value) && parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
}

function validateForm(form, submitButton, formErrorDiv) {
    submitButton.click(function () {
        var hasErrors = false;
        formErrorDiv.empty().hide();
        form.find('input[data-validator="integer"]').each(function (i, el) {
            if (!isInt($(el).val()) & $(el).val() != '') {
                hasErrors = true;
                $(el).parent().addClass('error');
                formErrorDiv.append('<span>' + $(el).attr('data-message') + '</span><br />').show();
            }
        });
        form.find('input[data-required="true"]').each(function (i, el) {
            if ($(el).val() == '') {
                hasErrors = true;
                $(el).parent().addClass('error');
                formErrorDiv.append('<span>' + $(el).attr('data-message') + '</span><br />').show();
            }
        });
        return !hasErrors;
    });
}

function getTableOrder(tableId) {
    var res = [];
    $('#' + tableId + ' tbody:first tr').each(function (i, el) {
        res.push({
            id: $(el).data('id'),
            order: i
        });
    });
    return res;
}

function minutesToTime(mins) {
    var hr = Math.floor(mins / 60);
    var mn = mins - 60 * hr;
    var res = mn.toString();
    if (res.length == 1) {
        res = "0" + res;
    }
    res = hr.toString() + ":" + res;
    return res;
}

if (!String.prototype.startsWith) {
    String.prototype.startsWith = function (searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
    };
}

function pad(number) {
    if (number < 10) {
        return '0' + number;
    }
    return number;
}

},{}],4:[function(require,module,exports){
"use strict";

module.exports = React.createClass({
    displayName: "exports",
    getInitialState: function getInitialState() {
        return {};
    },
    render: function render() {
        return React.createElement(
            "tr",
            null,
            React.createElement(
                "td",
                null,
                this.props.c.Criterium
            ),
            React.createElement(
                "td",
                null,
                this.props.c.Answer
            ),
            React.createElement(
                "td",
                null,
                React.createElement("input", {
                    type: "text",
                    ref: "numScore",
                    value: this.props.c.Score,
                    onChange: this.change
                })
            )
        );
    },
    change: function change() {

        this.props.onChange(this.props.i, this.refs.numScore.value);
    }
});

},{}],5:[function(require,module,exports){
"use strict";

module.exports = React.createClass({
    displayName: "exports",
    getInitialState: function getInitialState() {
        return {
            isDisqualified: this.props.itemCost.Disqualify
        };
    },
    render: function render() {
        return React.createElement(
            "tr",
            null,
            React.createElement(
                "td",
                null,
                this.props.itemCost.Description
            ),
            React.createElement(
                "td",
                null,
                this.props.itemCost.Cost
            ),
            React.createElement(
                "td",
                null,
                React.createElement("input", {
                    type: "checkbox",
                    checked: this.state.isDisqualified,
                    onChange: this.change
                })
            )
        );
    },
    change: function change() {
        this.setState({
            isDisqualified: !this.state.isDisqualified
        });

        this.props.onChange(this.props.index);
    }
});

},{}],6:[function(require,module,exports){
"use strict";

var CriteriumRow = require('./criteriumRow.jsx');

module.exports = React.createClass({
    displayName: "exports",
    getInitialState: function getInitialState() {
        var totalScore = 0;

        for (var i = 0; i < this.props.projectScoring.length; i++) {
            totalScore += this.props.projectScoring[i].Score;
        }

        return {
            projectScore: totalScore,
            projectScoring: this.props.projectScoring
        };
    },
    render: function render() {
        var _this = this;

        var criteria = this.state.projectScoring.map(function (item) {
            return React.createElement(CriteriumRow, { c: item, i: _this.state.projectScoring.indexOf(item), onChange: _this.scoreChanged });
        });

        return React.createElement(
            "div",
            null,
            React.createElement(
                "div",
                null,
                React.createElement(
                    "table",
                    { className: "table table-responsive table-striped" },
                    React.createElement(
                        "thead",
                        null,
                        React.createElement(
                            "tr",
                            null,
                            React.createElement(
                                "th",
                                null,
                                "Item"
                            ),
                            React.createElement(
                                "th",
                                null,
                                "Answer"
                            ),
                            React.createElement(
                                "th",
                                null,
                                "Score"
                            )
                        )
                    ),
                    React.createElement(
                        "tbody",
                        null,
                        criteria,
                        React.createElement(
                            "tr",
                            null,
                            React.createElement("td", null),
                            React.createElement(
                                "td",
                                null,
                                React.createElement(
                                    "b",
                                    null,
                                    "Total"
                                )
                            ),
                            React.createElement(
                                "td",
                                null,
                                this.state.projectScore
                            )
                        )
                    )
                )
            )
        );
    },
    scoreChanged: function scoreChanged(index, score) {
        this.state.projectScoring[index].Score = parseInt(score);

        var total = 0;

        for (var i = 0; i < this.state.projectScoring.length; i++) {
            total += this.state.projectScoring[i].Score;
        }

        this.setState({
            projectScore: total
        });
    }
});

},{"./criteriumRow.jsx":4}],7:[function(require,module,exports){
"use strict";

var ItemCostRow = require('./itemCostRow.jsx');

module.exports = React.createClass({
    displayName: "exports",
    getInitialState: function getInitialState() {
        var totalCost = 0;

        for (var i = 0; i < this.props.projectCosts.length; i++) {
            if (!this.props.projectCosts[i].Disqualify) {
                totalCost += this.props.projectCosts[i].Cost;
            }
        }

        return {
            qualifyingCost: totalCost,
            projectCosts: this.props.projectCosts
        };
    },
    render: function render() {
        var _this = this;

        var itemCostRows = this.state.projectCosts.map(function (item) {
            return React.createElement(ItemCostRow, { itemCost: item, index: _this.state.projectCosts.indexOf(item), onChange: _this.qualifyChanged });
        });

        return React.createElement(
            "div",
            null,
            React.createElement(
                "div",
                null,
                "Qualifying Cost (See ",
                React.createElement(
                    "a",
                    { href: "#", onClick: this.qualifyClick },
                    "Annex A"
                ),
                " to qualify/disqualify items)",
                React.createElement(
                    "p",
                    null,
                    this.state.qualifyingCost
                )
            ),
            React.createElement(
                "div",
                { className: "modal fade", id: "qualifyCosts", tabindex: "-1", role: "dialog", "aria-labelledby": "editRideLabel" },
                React.createElement(
                    "div",
                    { className: "modal-dialog", role: "document" },
                    React.createElement(
                        "div",
                        { className: "modal-content" },
                        React.createElement(
                            "div",
                            { className: "modal-header" },
                            React.createElement(
                                "button",
                                { type: "button", className: "close", "data-dismiss": "modal", "aria-label": "Close" },
                                React.createElement(
                                    "span",
                                    { "aria-hidden": "true" },
                                    "\xD7"
                                )
                            ),
                            React.createElement(
                                "h4",
                                { className: "modal-title", id: "annexALabel" },
                                "Annex A"
                            )
                        ),
                        React.createElement(
                            "div",
                            { className: "modal-body" },
                            React.createElement(
                                "table",
                                { className: "table table-responsive table-striped" },
                                React.createElement(
                                    "thead",
                                    null,
                                    React.createElement(
                                        "tr",
                                        null,
                                        React.createElement(
                                            "th",
                                            null,
                                            "Description"
                                        ),
                                        React.createElement(
                                            "th",
                                            null,
                                            "Cost"
                                        ),
                                        React.createElement(
                                            "th",
                                            null,
                                            "Disqualify"
                                        )
                                    )
                                ),
                                React.createElement(
                                    "tbody",
                                    null,
                                    itemCostRows
                                )
                            )
                        ),
                        React.createElement(
                            "div",
                            { className: "modal-footer" },
                            React.createElement(
                                "button",
                                { type: "button", className: "btn btn-default", "data-dismiss": "modal" },
                                "Cancel"
                            ),
                            React.createElement(
                                "button",
                                { type: "button", className: "btn btn-primary", onClick: this.updateCost },
                                "Save"
                            )
                        )
                    )
                )
            )
        );
    },
    qualifyClick: function qualifyClick(e) {
        e.preventDefault();
        $('#qualifyCosts').modal('show');
    },
    qualifyChanged: function qualifyChanged(i) {
        this.state.projectCosts[i].Disqualify = !this.state.projectCosts[i].Disqualify;
    },
    updateCost: function updateCost(e) {
        e.preventDefault();
        $('#qualifyCosts').modal('hide');

        var updatedCost = 0;

        for (var i = 0; i < this.state.projectCosts.length; i++) {
            if (!this.state.projectCosts[i].Disqualify) {
                updatedCost += this.state.projectCosts[i].Cost;
            }
        }

        this.setState({
            qualifyingCost: updatedCost
        });
    }
});

},{"./itemCostRow.jsx":5}],8:[function(require,module,exports){
'use strict';

window.AssessmentService = function ($, mid) {
    var moduleId = mid;
    var baseServicepath = $.dnnSF(moduleId).getServiceRoot('Assessment');

    this.ajaxCall = function (type, controller, action, id, data, success, fail) {
        // showLoading();
        $.ajax({
            type: type,
            url: baseServicepath + controller + '/' + action + (id != null ? '/' + id : ''),
            beforeSend: $.dnnSF(moduleId).setModuleHeaders,
            data: data
        }).done(function (retdata) {
            // hideLoading();
            if (success != undefined) {
                success(retdata);
            }
        }).fail(function (xhr, status) {
            // showError(xhr.responseText);
            if (fail != undefined) {
                fail(xhr.responseText);
            }
        });
    };

    this.save = function (assessment, success, fail) {
        this.ajaxCall('POST', 'Assessment', 'Save', null, assessment, success, fail);
    };
};

},{}]},{},[1,3,8])

