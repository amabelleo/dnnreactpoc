﻿using DotNetNuke.Web.Api;

namespace WS.Assessment.Api
{
    public class RouteMapper : IServiceRouteMapper
    {
        public void RegisterRoutes(IMapRoute mapRouteManager)
        {
            mapRouteManager.MapHttpRoute("Assessment", "AssessmentMap1", "{controller}/{action}", null, null, new[] { "WS.Assessment.Api" });
            mapRouteManager.MapHttpRoute("Assessment", "AssessmentMap2", "{controller}/{action}/{id}", null, new { id = "-?\\d+" }, new[] { "WS.Assessment.Api" });
        }
    }
}