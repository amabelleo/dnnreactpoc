﻿using DotNetNuke.Web.Api;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WS.Assessment.Common;
using WS.Assessment.Models;
using WS.Assessment.Repositories;

namespace WS.Assessment.Api
{
    public class AssessmentController : DnnApiController
    {
        public class SaveAssessmentDTO
        {
            public string ApplicationNumber { get; set; }
            public IEnumerable<ItemCost> ProjectCosts { get; set; }
            public IEnumerable<CriteriumScore> ProjectScoring { get; set; }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AssessmentAuthorize(SecurityLevel = SecurityAccessLevel.Assessor)]
        public HttpResponseMessage Save(SaveAssessmentDTO saveDto)
        {
            var assessment = AssessmentRepository.Instance.GetAssessment(1, saveDto.ApplicationNumber);

            assessment.AnnexA = saveDto.ProjectCosts;
            assessment.ProjectScoring = saveDto.ProjectScoring;

            AssessmentRepository.Instance.UpdateAssessment(saveDto.ApplicationNumber, assessment, UserInfo.UserID);

            return Request.CreateResponse(HttpStatusCode.OK, AssessmentRepository.Instance.GetAssessment(ActiveModule.ModuleID, saveDto.ApplicationNumber));
        }
    }
}