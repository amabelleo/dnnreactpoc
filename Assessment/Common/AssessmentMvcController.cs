﻿using DotNetNuke.Web.Mvc.Framework.Controllers;

namespace WS.Assessment.Common
{
    public class AssessmentMvcController : DnnController
    {
        private ContextHelper _AssessmentContext;

        public ContextHelper AssessmentContext
        {
            get { return _AssessmentContext ?? (_AssessmentContext = new ContextHelper(this)); }
        }
    }
}