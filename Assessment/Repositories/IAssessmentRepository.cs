﻿using System.Collections.Generic;
using WS.Assessment.Models;

namespace WS.Assessment.Repositories
{
    public interface IAssessmentRepository
    {
        ApplicationAssessment GetAssessment(int moduleId, string applicationNumber);
        void UpdateAssessment(string applicationNumber, ApplicationAssessment assessment, int userId);
        IEnumerable<ApplicationAssessment> GetApplications(int moduleId);
    }
}
