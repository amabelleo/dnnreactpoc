﻿using System;
using System.Collections.Generic;
using WS.Assessment.Models;
using DotNetNuke.Framework;

namespace WS.Assessment.Repositories
{
    public class AssessmentRepository : ServiceLocator<IAssessmentRepository, AssessmentRepository>, IAssessmentRepository
    {
        private IDictionary<string, ApplicationAssessment> assessments = new Dictionary<string, ApplicationAssessment>()
        {
            { "12345", new ApplicationAssessment
                {
                    ApplicationNumber = "12345",
                    ProjectName = "Test Project",
                    AnnexA = new List<ItemCost>
                    {
                        new ItemCost
                        {
                            Description = "Venue",
                            Cost = 3000
                        },
                        new ItemCost
                        {
                            Description = "Catering",
                            Cost = 12000
                        },
                        new ItemCost
                        {
                            Description = "Others: Alcohol",
                            Cost = 5000
                        }
                    },
                    ProjectScoring = new List<CriteriumScore>
                    {
                        new CriteriumScore
                        {
                            Criterium = "Enhancing Interaction and Social Relations",
                            Answer = "The workshops are designed to let participants learn more .."
                        },
                        new CriteriumScore
                        {
                            Criterium = "Encouraging Understanding of Singapore Society",
                            Answer = "Lorem ipsum dolor .."
                        },
                        new CriteriumScore
                        {
                            Criterium = "Connecting New Citizens, PRs to Singapore",
                            Answer = "Lorem ipsum dolor .."
                        }
                    },
                    Questions = new List<string>()
                }
            },
            { "12346", new ApplicationAssessment
                {
                    ApplicationNumber = "12346",
                    ProjectName = "Test Project 2",
                    AnnexA = new List<ItemCost>
                    {
                        new ItemCost
                        {
                            Description = "Venue",
                            Cost = 3000
                        },
                        new ItemCost
                        {
                            Description = "Catering",
                            Cost = 12000
                        },
                        new ItemCost
                        {
                            Description = "Entertainment",
                            Cost = 5000
                        },
                        new ItemCost
                        {
                            Description = "Others: Alcohol",
                            Cost = 5000
                        }
                    },
                    ProjectScoring = new List<CriteriumScore>
                    {
                        new CriteriumScore
                        {
                            Criterium = "Enhancing Interaction and Social Relations",
                            Answer = "The workshops are designed to let participants learn more .."
                        },
                        new CriteriumScore
                        {
                            Criterium = "Encouraging Understanding of Singapore Society",
                            Answer = "Lorem ipsum dolor .."
                        },
                        new CriteriumScore
                        {
                            Criterium = "Connecting New Citizens, PRs to Singapore",
                            Answer = "Lorem ipsum dolor .."
                        }
                    },
                    Questions = new List<string>()
                }
            }
        };

        protected override Func<IAssessmentRepository> GetFactory()
        {
            return () => new AssessmentRepository();
        }

        public ApplicationAssessment GetAssessment(int moduleId, string applicationNumber)
        {
            //using (var context = DataContext.Instance())
            //{
            //    var rep = context.GetRepository<Ride>();
            //    return rep.GetById(rideId, moduleId);
            //}
            return assessments[applicationNumber];
        }

        public void UpdateAssessment(string applicationNumber, ApplicationAssessment assessment, int userId)
        {
            assessments[applicationNumber] = assessment;
        }

        public IEnumerable<ApplicationAssessment> GetApplications(int moduleId)
        {
            return assessments.Values;
        }
    }
}