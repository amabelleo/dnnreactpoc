﻿using System.Collections.Generic;

namespace WS.Assessment.Models
{
    public class ApplicationAssessment 
    {
        public string ApplicationNumber { get; set; }
        public string ProjectName { get; set; }
        public IEnumerable<ItemCost> AnnexA { get; set; }
        public IEnumerable<CriteriumScore> ProjectScoring { get; set; }
        public IEnumerable<string> Questions { get; set; }
    }

    public class ItemCost
    {
        public string Description { get; set; }
        public int Cost { get; set; }
        public bool Disqualify { get; set; }
    }

    public class CriteriumScore
    {
        public string Criterium { get; set; }
        public string Answer { get; set; }
        public int Score { get; set; }
    }
}