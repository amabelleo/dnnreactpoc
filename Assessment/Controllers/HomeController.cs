﻿using System.Web.Mvc;
using WS.Assessment.Common;
using WS.Assessment.Repositories;

namespace WS.Assessment.Controllers
{
    public class HomeController : AssessmentMvcController
    {
        [HttpGet]
        public ActionResult Index()
        {
            var applications = AssessmentRepository.Instance.GetApplications(ModuleContext.ModuleId);
            return View(applications);
        }

        public ActionResult Assess(string id)
        {
            var application = AssessmentRepository.Instance.GetAssessment(ModuleContext.ModuleId, id);
            AssessmentContext.Application = application;
            return View();
        }
    }
}